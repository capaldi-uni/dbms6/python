__all__ = ['WarshipsFactory', 'ContentFactory']

from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker, Query

from sqlalchemy.engine import Engine, CursorResult
from sqlalchemy.orm.session import Session

from typing import Any, Optional

from .WarshipsSchema import *
from .ContentSchema import *


class Repository(object):
    tables = []
    _session: Optional[Session]

    def __init__(self, session: Session, commit_on_close=False):
        self._session = session
        self._coc = commit_on_close

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def __getitem__(self, item):
        if item in self.tables:
            return self.__getattribute__(item)

        raise KeyError(f'Table {item} does not exists')

    def close(self):
        if self._coc:
            self.session.commit()

        self.session.close()
        self._session = None

    def query(self, *entities, **kwargs) -> Query:
        return self.session.query(*entities, **kwargs)

    def add(self, instance):
        self.session.add(instance)

    def add_all(self, *instances):
        self.session.add_all(instances)

    def delete(self, instance):
        self.session.delete(instance)

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()

    @property
    def session(self) -> Session:
        if self._session and self._session.is_active:
            return self._session

        raise ConnectionError('Session already closed or not opened at all')


class WarshipsRepository(Repository):
    tables = ['User', 'Game', 'Privilege', 'Feedback',
              'Blacklist', 'Achievement', 'UsersToAchievements']

    # Entity classes
    Privilege = Privilege
    User = User
    Game = Game
    Feedback = Feedback
    Blacklist = Blacklist
    Achievement = Achievement
    UsersToAchievements = UsersToAchievements

    # Tables
    @property
    def users(self):
        return self.query(User)

    @property
    def privileges(self):
        return self.query(Privilege)

    @property
    def games(self):
        return self.query(Game)

    @property
    def feedback(self):
        return self.query(Feedback)

    @property
    def blacklist(self):
        return self.query(Blacklist)

    @property
    def achievements(self):
        return self.query(Achievement)

    @property
    def achievement_records(self):
        return self.query(UsersToAchievements)

    # Queries
    def get_relevant_feedback(self, min_level=0) -> CursorResult:
        sql = """
        SELECT
            users.id,
            users.nickname,
            users.level,
            feedback.text
        FROM
            users
        JOIN feedback ON
            users.id = user_id
        WHERE
            "level" >= :level
            AND users.last_ip NOT IN (
            SELECT
                ip
            FROM
                blacklist);
        """

        return self.session.execute(text(sql), {'level': min_level})

    def get_game_count(self):
        sql = """
        SELECT
            COUNT(game_id),
            users.id,
            users.nickname
        FROM
            users
        JOIN games ON
            users.id = user_id
        GROUP BY
            users.id
        ORDER BY
            COUNT(game_id) DESC;
        """

        return self.session.execute(text(sql))

    def get_ban_reasons(self):
        sql = """
        SELECT
            nickname,
            reason
        FROM
            blacklist
        JOIN users ON
            users.last_ip = blacklist.ip;
        """

        return self.session.execute(text(sql))

    def user_exists(self, name='Nik'):
        sql = """
        SELECT
            EXISTS(
            SELECT
                id
            FROM
                users
            WHERE
                nickname = :name);
        """

        return next(self.session.execute(text(sql), {'name': name}))[0]


class ContentRepository(Repository):
    tables = ['Ship', 'Deck',  'Environment', 'Color', 'Armor',
              'ArmorType', 'Ammunition', 'AmmunitionType', ]

    # Entity classes
    Color = Color
    Armor = Armor
    ArmorType = ArmorType
    Ammunition = Ammunition
    AmmunitionType = AmmunitionType
    Environment = Environment
    Deck = Deck
    Ship = Ship

    # Tables
    @property
    def colors(self):
        return self.query(Color)

    @property
    def armor(self):
        return self.query(Armor)

    @property
    def armor_types(self):
        return self.query(ArmorType)

    @property
    def ammunition(self):
        return self.query(Ammunition)

    @property
    def ammunition_types(self):
        return self.query(AmmunitionType)

    @property
    def ships(self):
        return self.query(Ship)

    @property
    def environments(self):
        return self.query(Environment)

    @property
    def decks(self):
        return self.query(Deck)

    # Queries
    def get_ships_of_color(self, color='Black'):
        sql = """
         SELECT
            DISTINCT ship.name
        FROM
            ship
        JOIN ship_to_deck ON
            ship.id = ship_id
        JOIN deck ON
            deck_id = deck.id
        JOIN color ON
            color.id = color_id
        WHERE
            color.name = :name;
        """

        if isinstance(color, self.Color):
            name = color.name
        else:
            name = color

        return self.session.execute(text(sql), {'name': name})

    def ammunition_of_ship(self, ship='Vampire'):
        sql = """
         SELECT
            ammunition.id,
            damage,
            aoe,
            ammunition_count,
            ammunition_type.name
        FROM
            ship
        JOIN ship_to_deck ON
            ship.id = ship_id
        JOIN deck ON
            deck_id = deck.id
        JOIN ammunition ON
            ammunition_id = ammunition.id
        JOIN ammunition_type ON
            type_id = ammunition_type.id
        WHERE
            ship.name = :name;
        """

        if isinstance(ship, self.Ship):
            name = ship.name
        else:
            name = ship

        return self.session.execute(text(sql), {'name': name})

    def ships_of_environment(self, env='Caribbean'):
        sql = """
                 SELECT
            ship.name
        FROM
            ship
        JOIN environment_to_ship ON
            ship.id = ship_id
        JOIN environment ON
            environment_id = environment.id
        WHERE
            environment.name = :name;
        """

        if isinstance(env, self.Environment):
            name = env.name
        else:
            name = env

        return self.session.execute(text(sql), {'name': name})


class RepositoryFactory(object):
    _Session: sessionmaker
    _engine: Engine

    _Repository = Repository

    def __init__(self, url, commit_on_close=False):
        self._engine = create_engine(url)
        self._Session = sessionmaker(bind=self._engine)

        self._coc = commit_on_close

    def __call__(self) -> Any:
        return self._Repository(self._Session(), self._coc)


class WarshipsFactory(RepositoryFactory):
    _Repository = WarshipsRepository

    def __init__(self, *args, **kwargs):
        url = 'postgresql://postgres:admin@localhost:5432/warships'

        super().__init__(url, *args, **kwargs)

    def __call__(self) -> _Repository:
        return super().__call__()


class ContentFactory(RepositoryFactory):
    _Repository = ContentRepository

    def __init__(self, *args, **kwargs):
        url = 'mssql+pymssql://localhost:1433/warships_settings'

        super().__init__(url, *args, **kwargs)

    def __call__(self) -> _Repository:
        return super().__call__()
