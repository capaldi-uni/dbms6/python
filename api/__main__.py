from . import WarshipsFactory, ContentFactory, LogRepository

import pandas as pd

from .db_filler import fill


def df_from_cursor(cur):
    data = [{key: row[key] for key in row.keys()} for row in cur]

    if data:
        return pd.DataFrame(data)

    raise ValueError('Cursor is empty')


def df_from_list(obj):
    data = [item.dict for item in obj]

    if data:
        return pd.DataFrame(data)

    raise ValueError('No objects found')


def get_int(prompt='', mx=None, mn=None):
    while True:
        try:
            res = int(input(prompt))

        except ValueError:
            print('Please enter a number')

        else:
            if mx and res > mx:
                print('Value is too big!')

            elif mn and res < mn:
                print('Value is too small')

            else:
                return res


def confirm(message, prompt=''):
    print(f'{message} (y/n)')
    cf = input(prompt)

    if cf.lower()[0] == 'y':
        return True

    return False


Warships = WarshipsFactory()
Content = ContentFactory()
Log = LogRepository()

admin = 'admin > '
conn = None

# Main loop - Database selection
while True:
    print('Select database:')
    print('0 - Exit\n'
          '1 - Warships\n'
          '2 - Content\n'
          '3 - Log\n')

    db_select = get_int(admin, 3, 0)

    if db_select == 0:
        break

    if db_select == 1:
        conn = Warships()

    elif db_select == 2:
        conn = Content()

    else:
        while True:
            with Warships() as ws, Content() as ct, Log(ws, ct):
                print('-- Games log --')
                with pd.option_context('display.max_columns', None,
                                       'display.max_colwidth', -1,
                                       'display.expand_frame_repr', False):
                    print(df_from_list(Log.games))
                    print()

                print('What do you want to do?')
                print('0 - Back\n'
                      '1 - Find longest game\n'
                      '2 - Find game with least turns\n'
                      '3 - View ship placement\n'
                      '4 - View game log\n'
                      '5 - Delete game\n'
                      '6 - Fill games\n')

                action = get_int(admin, 6, 0)

                if action == 0:
                    break

                if action == 1:
                    game = Log.find_longest_game()
                    print(f'{game} is the longest. It lasted for {game.len}\n')
                    continue

                if action == 2:
                    game = Log.find_least_turns()
                    print(f'{game} has the least turns - {len(game.turns)}\n')
                    continue

                if action == 6:
                    print('Enter number of records to add:')
                    count = get_int(admin, 20, 0)

                    fill(count)

                    print(f'{count} games was added')
                    continue

                print('Select game:')
                game_num = get_int(admin, len(Log.games), 0)
                game: Log.Game = Log.games[game_num]

                if action == 3:
                    print('-- Ship placement for Player 1 --')
                    print(df_from_list(game.player1_field))
                    print()

                    print('-- Ship placement for Player 2 --')
                    print(df_from_list(game.player2_field))
                    print()

                if action == 4:
                    print('-- Game log --')
                    print(df_from_list(game.turns))
                    print()

                if action == 5:
                    if confirm(f'Are you sure you want '
                               f'to delete {game}?', admin):

                        for g in ws.games.where(
                                ws.Game.game_id == str(game.id)).all():

                            ws.delete(g)

                        Log.delete_game(game)

                        ws.commit()
                        print(f'{game} was deleted')

        continue

    # Table selection
    while True:
        print('Select table:')
        print('0 - Back\n'
              '1 - Requests\n'
              + '-' * 10)

        names = []

        i = 1
        for i, table_name in enumerate(conn.tables, 2):
            names.append(f'{i} - {table_name}')

        print('\n'.join(names))
        print()

        table_select = get_int(admin, i, 0)

        if table_select == 0:
            break

        # Requests
        elif table_select == 1:
            while True:
                print('Select query:')

                # For warships
                if db_select == 1:
                    print('0 - Back\n'
                          '1 - Get relevant feedback\n'
                          '2 - Get game counts for users\n'
                          '3 - Get ban reasons for blacklisted users\n'
                          '4 - Check if user with nickname exists\n')

                # For content
                if db_select == 2:
                    print('0 - Back\n'
                          '1 - View ammunition of ship\n'
                          '2 - View ships of given color\n'
                          '3 - View ships of given environment\n')

                request = get_int(admin, 5 - db_select, 0)

                if request == 0:
                    break

                # For warships
                if db_select == 1:
                    if request == 1:
                        cursor = conn.get_relevant_feedback()
                    elif request == 2:
                        cursor = conn.get_game_count()
                    elif request == 3:
                        cursor = conn.get_ban_reasons()
                    else:
                        print('Enter username:')
                        nickname = input(admin)

                        if conn.user_exists(nickname):
                            print(f'User `{nickname}` exists\n')

                        else:
                            print(f'User `{nickname}` does not exist\n')

                        continue

                    try:
                        print(df_from_cursor(cursor))
                        print()

                    except ValueError:
                        print('Result set is empty\n')

                # For content
                if db_select == 2:
                    if request == 1:
                        print('Enter ship name:')
                        name = input()
                        cursor = conn.ammunition_of_ship(name)
                    elif request == 2:
                        print('Enter color name:')
                        name = input()
                        cursor = conn.get_ships_of_color(name)
                    else:
                        print('Enter name of environment:')
                        name = input()
                        cursor = conn.ships_of_environment(name)

                    try:
                        print(df_from_cursor(cursor))
                        print()

                    except ValueError:
                        print('Result set is empty\n')

        # Tables
        else:
            table_select -= 2
            table_name = conn.tables[table_select]

            table = conn[table_name]

            edited = False

            # Table view and action selection
            while True:
                print(f'\n-- Table {table_name} --')
                records = conn.query(table).all()

                if records:
                    print(pd.DataFrame([record.dict for record in records]))
                    print()
                else:
                    print(f'Table {table_name} is empty\n')

                print('What do you want to do?')
                if records:
                    print('0 - Back\n'
                          '1 - Edit record\n'
                          '2 - Add record\n'
                          '3 - Delete record\n')

                    c = get_int(admin, 3, 0)

                else:
                    print('0 - Back\n'
                          '1 - Add record\n')

                    c = get_int(admin, 1, 0)

                    if c == 1:
                        c = 2

                # Check for changes on exit and prompt to save
                if c == 0:
                    if edited:
                        if confirm('Save changes?', admin):
                            conn.commit()
                            print('All changes saved')
                        else:
                            conn.rollback()
                            print('All changes reverted')

                    break

                # Edit record
                if c == 1:
                    print('Select record (-1 to exit): ')
                    try:
                        record_number = get_int(admin)

                        if record_number < 0:
                            continue

                        record = records[record_number]

                    except (ValueError, IndexError):
                        print('No such record!')
                        continue

                    fields = [column for column
                              in record.__table__.c
                              if column.name != 'id']

                    # Select field and new value for it
                    while True:
                        print(f'Record {record}')
                        print('Select field (-1 to exit):')
                        print(*[f'{i} - {field.name}' for i, field
                                in enumerate(fields)], sep='\n')

                        try:
                            field_number = get_int(admin)

                            if field_number < 0:
                                break

                            field = fields[field_number]

                        except (ValueError, IndexError):
                            print('No such field!')
                            continue

                        field_type = field.type

                        try:
                            field_type_name = field_type.__name__
                        except AttributeError:
                            field_type_name = field_type.__class__.__name__

                        print(f'Current value of {field.name}: '
                              f'{record[field.name]}')

                        print(f'Enter new value for {field.name}'
                              f'({field_type_name}):')

                        new_value_str = input(admin)

                        try:
                            new_value = field.type.python_type(new_value_str)
                            record[field.name] = new_value
                            edited = True

                        except ValueError:
                            print(f'Cannot convert to {field_type_name}!')

                        break

                # Add record
                if c == 2:
                    fields = [column for column
                              in table.__table__.c
                              if column.name != 'id']

                    values = {}

                    print(f'Creating new {table_name}')

                    for field in fields:
                        field_type = field.type

                        try:
                            field_type_name = field_type.__name__
                        except AttributeError:
                            field_type_name = field_type.__class__.__name__

                        while True:
                            print(f'Enter value for {field.name}'
                                  f'({field_type_name}):')

                            new_value_str = input(admin)

                            try:
                                new_value = field.type.python_type(new_value_str)

                                values[field.name] = new_value

                                break

                            except ValueError:
                                print(f'Cannot convert to {field_type_name}!')

                    record = table(**values)
                    conn.add(record)
                    edited = True

                    print(f'{table_name} {record} was created')

                # Delete record
                if c == 3:
                    print('Select record (-1 to exit): ')
                    try:
                        record_number = get_int(admin)

                        if record_number == -1:
                            continue

                        record = records[record_number]

                    except (ValueError, IndexError):
                        print('No such record!')
                        continue

                    if record.can_delete:
                        if confirm(f'Are you sure you want to '
                                   f'delete {record} {table_name}?', admin):

                            conn.delete(record)
                            edited = True
                            print(f'{table_name} {record} was deleted')
                        else:
                            print('Aborting')
                            continue

                    else:
                        print(f'Cannot delete {table_name} {record}, '
                              f'because it has records linked to it')

    conn.close()
