__all__ = ['LogRepository']

from mongoengine import Document, ValidationError, \
    IntField, DateTimeField, ListField, ReferenceField, \
    connect as _connect, disconnect as _disconnect

from datetime import datetime, timedelta

from .Repositories import WarshipsRepository, ContentRepository

DEFAULT_DATABASE = 'warships'


class Ship(Document):
    meta = {"db_alias": DEFAULT_DATABASE}

    ct: ContentRepository = None
    ws: WarshipsRepository = None

    ship_id = IntField(0, required=True)

    x = IntField(0, required=True)
    y = IntField(0, required=True)

    rotation = IntField(default=12, choices=(3, 6, 9, 12), required=True)

    @property
    def ship(self):
        if self.ct:
            return self.ct.ships.where(self.ct.Ship.id == self.ship_id).one()

        return self.ship_id

    @ship.setter
    def ship(self, ship):
        self.ship_id = ship.id

    def __repr__(self):
        return f'Ship({self.ship_id} at ({self.x}, {self.y}) ' \
               f'{self.rotation} o\'clock)'

    __str__ = __repr__

    @property
    def dict(self):
        return {
            'id': str(self.id),
            'ship': str(self.ship),
            'pos': (self.x, self.y),
            'rotation': self.rotation
        }

    @classmethod
    def field_names(cls):
        return {'ship_id', 'x', 'y', 'rotation'}


class Turn(Document):
    meta = {"db_alias": DEFAULT_DATABASE}

    ct: ContentRepository = None
    ws: WarshipsRepository = None

    player_id = IntField(1, required=True)

    attacker_x = IntField(0, required=True)
    attacker_y = IntField(0, required=True)

    target_x = IntField(0, required=True)
    target_y = IntField(0, required=True)

    timestamp = DateTimeField(required=True)

    def clean(self):
        if not self.timestamp:
            self.timestamp = datetime.utcnow()

    @property
    def player(self):
        if self.ct:
            return self.ws.users.where(self.ws.User.id == self.player_id).one()

        return self.player_id

    @player.setter
    def player(self, user):
        self.player_id = user.id

    def __repr__(self):
        return f'Turn({self.player_id}, ' \
               f'({self.attacker_x}, {self.attacker_y}) -> ' \
               f'({self.target_x}, {self.target_y}))'

    __str__ = __repr__

    @property
    def dict(self):
        return {
            'id': str(self.id),
            'player': str(self.player),
            'from': (self.attacker_x, self.attacker_y),
            'to': (self.target_x, self.target_y)
        }

    @classmethod
    def field_names(cls):
        return {'player_id', 'attacker_x', 'attacker_y',
                'target_x', 'target_y'}


class Game(Document):
    meta = {"db_alias": DEFAULT_DATABASE}

    ct: ContentRepository = None
    ws: WarshipsRepository = None

    environment = IntField(0, required=True)

    player1_id = IntField(1, required=True)
    player2_id = IntField(1, required=True)

    starting_player = IntField(default=0, choices=(0, 1), required=True)
    start_time = DateTimeField(required=True)

    player1_field = ListField(ReferenceField('Ship', reverse_delete_rule=3))
    player2_field = ListField(ReferenceField('Ship', reverse_delete_rule=3))

    turns = ListField(ReferenceField('Turn', reverse_delete_rule=3))

    def clean(self):
        if self.player1_id == self.player2_id:
            raise ValidationError('Player 1 must not be also a Player 2')

        if not self.start_time:
            self.start_time = datetime.utcnow()

    @property
    def player1(self):
        if self.ws:
            return self.ws.users.where(self.ws.User.id == self.player1_id).one()

        return self.player1_id

    @player1.setter
    def player1(self, user):
        self.player1_id = user.id

    @property
    def player2(self):
        if self.ws:
            return self.ws.users.where(self.ws.User.id == self.player2_id).one()

        return self.player2_id

    @player2.setter
    def player2(self, user):
        self.player2_id = user.id

    def __repr__(self):
        return f'Game({self.player1} vs {self.player2}, ' \
               f'{self.start_time:%d.%m.%Y %H:%M:%S})'

    __str__ = __repr__

    @property
    def dict(self):
        return {
            'id': str(self.id),
            'player1': str(self.player1),
            'player2': str(self.player2),

            'starting': str(self.player2
                            if self.starting_player
                            else self.player1),

            'start_time': self.start_time.strftime('%d.%m.%Y %H:%M:%S')
        }

    @property
    def len(self) -> timedelta:
        return self.turns[-1].timestamp - self.start_time

    @classmethod
    def field_names(cls):
        return {'environment', 'player1_id', 'player2_id', 'starting_player'}


class LogRepository(object):
    tables = ['Game', 'Ship', 'Turn']

    Ship = Ship
    Turn = Turn
    Game = Game

    connections = 0

    def __call__(self, ws=None, ct=None):
        for table in [self.Ship, self.Turn, self.Game]:
            table.ws = ws
            table.ct = ct

        return self

    def __enter__(self):
        if self.connections == 0:
            _connect(DEFAULT_DATABASE, DEFAULT_DATABASE)

        self.connections += 1

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connections -= 1

        if not self.connections:
            _disconnect(DEFAULT_DATABASE)

        for table in [self.Ship, self.Turn, self.Game]:
            table.ws = None
            table.ct = None

    def __getitem__(self, item):
        if item in self.tables:
            return self.__getattribute__(item)

        raise KeyError(f'Table {item} does not exists')

    @property
    def games(self):
        return self.Game.objects

    def drop_all(self):
        self.Game.drop_collection()
        self.Turn.drop_collection()
        self.Ship.drop_collection()

    @staticmethod
    def delete_game(game: Game):
        turns = list(game.turns)
        ships1 = list(game.player1_field)
        ships2 = list(game.player2_field)

        game.delete()

        for turn in turns:
            turn.delete()

        for ship in ships1:
            ship.delete()

        for ship in ships2:
            ship.delete()

    # Queries ?
    def find_longest_game(self) -> Game:
        return sorted(self.games, key=lambda g: g.len, reverse=True)[0]

    def find_least_turns(self) -> Game:
        return sorted(self.games, key=lambda g: len(g.turns))[0]


if __name__ == '__main__':
    Log = LogRepository()

    with Log:
        ...
