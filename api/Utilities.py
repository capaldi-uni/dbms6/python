class ItemAttrMixin:
    def __getitem__(self, item):
        return self.__getattribute__(item)

    def __setitem__(self, key, value):
        return self.__setattr__(key, value)


class GetFieldNamesMixin:
    @classmethod
    def fields(cls):
        try:
            return {f for f in cls.__table__.c if f.name != 'id'}
        except AttributeError as e:
            raise AttributeError('Required attribute __table__ not found. '
                                 'Is this not mapping class?') from e

    @classmethod
    def field_names(cls):
        try:
            return {f.name for f in cls.__table__.c if f.name != 'id'}
        except AttributeError as e:
            raise AttributeError('Required attribute __table__ not found. '
                                 'Is this not mapping class?') from e
