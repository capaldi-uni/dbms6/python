__all__ = [
    'Color',
    'Armor',
    'ArmorType',
    'Ammunition',
    'AmmunitionType',
    'Ship',
    'Environment',
    'Deck'
]

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Table, Column, ForeignKey, Integer, VARCHAR

from .Utilities import ItemAttrMixin, GetFieldNamesMixin

v255 = VARCHAR(255)


# This base let's us declare all the stuff
Base = declarative_base()


# Many to many tables - do not need special mappings
ShipToDeck = Table(
    'ship_to_deck', Base.metadata,
    Column('id', Integer, primary_key=True),
    Column('ship_id', Integer, ForeignKey('ship.id'), nullable=False),
    Column('deck_id', Integer, ForeignKey('deck.id'), nullable=False)
)

EnvironmentToShip = Table(
    'environment_to_ship', Base.metadata,
    Column('ship_id', Integer, ForeignKey('ship.id'), primary_key=True),
    Column('environment_id', Integer,
           ForeignKey('environment.id'), primary_key=True)
)


class Color(Base, ItemAttrMixin, GetFieldNamesMixin):
    """Color in RGB(A) format"""
    __tablename__ = 'color'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', v255, nullable=False)

    R = Column('R', Integer, default=0, nullable=False)
    G = Column('G', Integer, default=0, nullable=False)
    B = Column('B', Integer, default=0, nullable=False)
    A = Column('A', Integer, default=100, nullable=False)

    decks = relationship('Deck', back_populates='color')

    @property
    def rgb(self):
        return self.R, self.G, self.B

    @rgb.setter
    def rgb(self, rgb):
        self.R, self.G, self.B = rgb
        self.A = 100

    @property
    def rgba(self):
        return self.R, self.G, self.B, self.A

    @rgba.setter
    def rgba(self, rgba):
        self.R, self.G, self.B, self.A = rgba

    def __repr__(self):
        return f'<Color{self.rgba} -> {self.name}>'

    def __str__(self):
        return self.name

    @property
    def dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'R': self.R,
            'G': self.G,
            'B': self.B,
            'A': self.A
        }

    @property
    def can_delete(self):
        return not (self.decks or self.waters or self.earths)


class AmmunitionType(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'ammunition_type'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', v255, nullable=False)

    ammunition = relationship('Ammunition', back_populates='type')

    def __repr__(self):
        return f'<AmmunitionType({self.name})>'

    def __str__(self):
        return self.name

    @property
    def dict(self):
        return {
            'id': self.id,
            'name': self.name
        }

    @property
    def can_delete(self):
        return not self.ammunition


class Ammunition(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'ammunition'

    id = Column('id', Integer, primary_key=True)
    damage = Column('damage', Integer, nullable=False)
    aoe = Column('aoe', Integer, nullable=False)

    type_id = Column('type_id', Integer,
                     ForeignKey('ammunition_type.id'), nullable=False)

    type = relationship('AmmunitionType', back_populates='ammunition')

    decks = relationship('Deck', back_populates='ammunition')

    def __repr__(self):
        return f'<Ammunition({repr(self.type)}, {self.damage}, {self.aoe})>'

    def __str__(self):
        return f'{self.damage} dmg, {self.type}'

    @property
    def dict(self):
        return {
            'id': self.id,
            'damage': self.damage,
            'aoe': self.aoe,
            'type': str(self.type)
        }

    @property
    def can_delete(self):
        return not self.decks


class ArmorType(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'armor_type'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', v255, nullable=False)

    armor = relationship('Armor', back_populates='type')

    def __repr__(self):
        return f'<ArmorType({self.name})>'

    def __str__(self):
        return self.name

    @property
    def dict(self):
        return {
            'id': self.id,
            'name': self.name
        }

    @property
    def can_delete(self):
        return not self.armor


class Armor(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'armor'

    id = Column('id', Integer, primary_key=True)
    value = Column('value', Integer, nullable=False)

    type_id = Column('type_id', Integer,
                     ForeignKey('armor_type.id'), nullable=False)

    type = relationship('ArmorType', back_populates='armor')

    decks = relationship('Deck', back_populates='armor')

    def __repr__(self):
        return f'<Armor({self.type}, {self.value})>'

    def __str__(self):
        return f'{self.value} arm, {self.type}'

    @property
    def dict(self):
        return {
            'id': self.id,
            'value': self.value,
            'type': str(self.type)
        }

    @property
    def can_delete(self):
        return not self.decks


class Environment(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'environment'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', v255, nullable=False)
    field_size = Column('field_size', Integer, nullable=False)

    water_color_id = Column('water_color_id', Integer,
                            ForeignKey('color.id'), nullable=False)

    earth_color_id = Column('earth_color_id', Integer,
                            ForeignKey('color.id'), nullable=False)

    water_color = relationship('Color', foreign_keys=[water_color_id],
                               backref='waters')

    earth_color = relationship('Color', foreign_keys=[earth_color_id],
                               backref='earths')

    ships = relationship('Ship', secondary='environment_to_ship',
                         back_populates='environments')

    def __repr__(self):
        return f'<Environment({self.name})>'

    def __str__(self):
        return self.name

    @property
    def dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'field_size': self.field_size,

            'water_color': str(self.water_color),
            'earth_color': str(self.earth_color)
        }

    @property
    def can_delete(self):
        return not self.ships   # True???


class Deck(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'deck'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', v255, nullable=False)

    crew_count = Column('crew_count', Integer, nullable=False)
    default_life = Column('default_life', Integer, nullable=False)
    ammunition_count = Column('ammunition_count', Integer, nullable=False)

    color_id = Column('color_id', Integer,
                      ForeignKey('color.id'), nullable=False)

    armor_id = Column('armor_id', Integer,
                      ForeignKey('armor.id'), nullable=False)

    ammunition_id = Column('ammunition_id', Integer,
                           ForeignKey('ammunition.id'), nullable=False)

    color = relationship('Color', back_populates='decks')

    armor = relationship('Armor', back_populates='decks')
    ammunition = relationship('Ammunition', back_populates='decks')

    ships = relationship('Ship', secondary='ship_to_deck',
                         back_populates='decks')

    def __repr__(self):
        return f'<Deck({self.name})>'

    def __str__(self):
        return self.name

    @property
    def dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'crew_count': self.crew_count,
            'default_life': self.default_life,
            'ammunition_count': self.ammunition_count,

            'color': str(self.color),
            'armor': str(self.armor),
            'ammunition': str(self.ammunition),
        }

    @property
    def can_delete(self):
        return not self.ships


class Ship(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'ship'

    id = Column('id', Integer, primary_key=True)
    name = Column('name', v255, nullable=False)

    decks = relationship('Deck', secondary='ship_to_deck',
                         back_populates='ships')

    environments = relationship('Environment', secondary='environment_to_ship',
                                back_populates='ships')

    def __repr__(self):
        return f'<Ship({self.name})>'

    def __str__(self):
        return self.name

    @property
    def dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'decks': [str(deck) for deck in self.decks]
        }

    @property
    def can_delete(self):
        return True     # ?????
