__all__ = [
    'Privilege',
    'User',
    'Game',
    'Feedback',
    'Blacklist',
    'Achievement',
    'UsersToAchievements'
]

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, ForeignKey, \
    Integer, VARCHAR, CHAR, TIMESTAMP, LargeBinary

from .Utilities import ItemAttrMixin, GetFieldNamesMixin

import hashlib

Base = declarative_base()


class Privilege(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'privileges'
    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR(32), nullable=False)

    users = relationship('User', back_populates='privilege')

    def __repr__(self):
        return f'Privilege({self.name})'

    def __str__(self):
        return self.name

    @property
    def dict(self):
        return {
            'id': self.id,
            'name': self.name
        }

    @property
    def can_delete(self):
        return not self.users


class User(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'users'
    id = Column('id', Integer, primary_key=True)
    nickname = Column('nickname', VARCHAR(32), nullable=False)
    pass_hash = Column('pass_hash', LargeBinary, nullable=False)
    last_ip = Column('last_ip', VARCHAR(16), nullable=False)
    privilege_id = Column('privilege_id', Integer, ForeignKey('privileges.id'), nullable=False)
    experience = Column('experience', Integer, nullable=False)
    level = Column('level', Integer, nullable=False)

    privilege = relationship('Privilege', back_populates='users')
    games = relationship('Game', back_populates='user')
    feedback = relationship('Feedback', back_populates='user')
    bans_issued = relationship('Blacklist', back_populates='issuer')
    achievement_records = relationship('UsersToAchievements', back_populates='user')

    def __repr__(self):
        return f'User({self.nickname}, {self.privilege}, {self.last_ip})'

    def __str__(self):
        return self.nickname

    @property
    def dict(self):
        return {
            'id': self.id,
            'nickname': self.nickname,
            'pass_hash': self.pass_hash,
            'last_ip': self.last_ip,
            'privilege': str(self.privilege),
            'experience': self.experience,
            'level': self.level
        }

    @classmethod
    def new(cls, nickname, password):
        user = User(nickname=nickname, privilege_id=1,
                    experience=0, level=1, last_ip='127.0.0.1')

        user.set_password(password)

        return user

    @property
    def can_delete(self):
        return not (self.games or self.feedback
                    or self.bans_issued or self.achievement_records)

    def set_password(self, password: str):
        hashed = self._hash_password(password)
        self.pass_hash = hashed

    def check_password(self, password: str):
        return self.pass_hash == self._hash_password(password)

    def _hash_password(self, password: str):
        hash_obj = hashlib.sha256(password.encode())    # hash
        hash_obj.update(self.nickname.encode())     # personal salt
        hash_obj.update(b'imagine dragons')     # universal salt

        return hash_obj.digest()


class Game(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'games'
    user_id = Column('user_id', Integer, ForeignKey('users.id'), primary_key=True)
    game_id = Column('game_id', CHAR(24), primary_key=True)

    user = relationship('User', back_populates='games')

    def __repr__(self):
        return f'Game({self.user}, {self.game_id})'

    def __str__(self):
        return f'{self.user}, {self.game_id}'

    @property
    def dict(self):
        return {
            'user': str(self.user),
            'game_id': self.game_id
        }

    @property
    def can_delete(self):
        return True


class Feedback(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'feedback'
    id = Column('id', Integer, primary_key=True)
    user_id = Column('user_id', Integer, ForeignKey('users.id'), nullable=False)
    text = Column('text', VARCHAR(255), nullable=False)

    user = relationship('User', back_populates='feedback')

    def __repr__(self):
        return f'Feedback({self.user}: {self.text})'

    def __str__(self):
        return f'{self.user}, {self.text}'

    @property
    def dict(self):
        return {
            'id': self.id,
            'user': str(self.user),
            'text': self.text
        }

    @property
    def can_delete(self):
        return True


class Blacklist(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'blacklist'
    id = Column('id', Integer, primary_key=True)
    ip = Column('ip', VARCHAR(16), nullable=False)
    reason = Column('reason', VARCHAR(255), nullable=False)
    issuer_id = Column('issuer_id', Integer, ForeignKey('users.id'), nullable=False)

    issuer = relationship('User', back_populates='bans_issued')

    def __repr__(self):
        return f'Blacklist({self.ip}, {self.reason})'

    def __str__(self):
        return self.ip

    @property
    def dict(self):
        return {
            'id': self.id,
            'ip': self.ip,
            'reason': self.reason,
            'issuer': str(self.issuer)
        }

    @property
    def can_delete(self):
        return True


class Achievement(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'achievements'
    id = Column('id', Integer, primary_key=True)
    name = Column('name', VARCHAR(255), nullable=False)
    icon = Column('icon', VARCHAR(255), nullable=False)
    asset_id = Column('asset_id', VARCHAR(255), nullable=False)

    achievement_records = relationship('UsersToAchievements', back_populates='achievement')

    def __repr__(self):
        return f'Achievement({self.name})'

    def __str__(self):
        return self.name

    @property
    def dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'icon': self.icon,
            'asset_id': self.asset_id
        }

    @property
    def can_delete(self):
        return not self.achievement_records


class UsersToAchievements(Base, ItemAttrMixin, GetFieldNamesMixin):
    __tablename__ = 'users_to_achievements'
    user_id = Column('user_id', Integer, ForeignKey('users.id'), primary_key=True)
    achievement_id = Column('achievement_id', Integer,  ForeignKey('achievements.id'), primary_key=True)
    acquirement_date = Column('acquirement_date', TIMESTAMP, nullable=False)

    user = relationship('User', back_populates='achievement_records')
    achievement = relationship('Achievement', back_populates='achievement_records')

    def __repr__(self):
        return f'UsersToAchievements({self.achievement}, {self.user}, {self.acquirement_date})'

    def __str__(self):
        return f'{self.achievement} got {self.user} at {self.acquirement_date}'

    @property
    def dict(self):
        return {
            'user': str(self.user),
            'achievement': str(self.achievement),
            'acquirement_date': self.acquirement_date
        }

    @property
    def can_delete(self):
        return True
