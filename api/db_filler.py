from . import WarshipsFactory, ContentFactory
from .Mongo import LogRepository

import datetime

import random
import sys


def random_date():
    return datetime.datetime.utcnow() - \
        datetime.timedelta(seconds=random.randint(0, 864_000))


def random_turn(start):
    return start + datetime.timedelta(seconds=random.randint(5, 60))


def fill(num=10, clean=False, seed=None):
    if seed is None:
        # 3979739643735396750
        seed = random.randrange(sys.maxsize)

    random.seed(seed)
    print(seed)

    Logs = LogRepository()
    Warships = WarshipsFactory()
    Content = ContentFactory()

    with Warships() as ws, Content() as ct, Logs(ws, ct):
        if clean:
            Logs.drop_all()
            for g in ws.games.all():
                ws.delete(g)

            ws.commit()

        print('Starting')
        for _ in range(num):
            print(f'Making game {_}')

            player1, player2 = None, None

            while player1 == player2:
                player1, player2 = random.choices(ws.users.all(), k=2)
                players = [player1, player2]

            print(players)

            env = random.choice(ct.environments.all())

            print(env)

            fs = env.field_size - 1

            starting_player = random.randint(0, 1)

            start_time = random_date()

            p1_ships = [Logs.Ship(ship_id=random.choice(ct.ships.all()).id,
                                  x=random.randint(0, fs),
                                  y=random.randint(0, fs),
                                  rotation=random.choice((3, 6, 9, 12))
                                  ) for _ in range(10)]

            for s in p1_ships:
                s.save()

            p2_ships = [Logs.Ship(ship_id=random.choice(ct.ships.all()).id,
                                  x=random.randint(0, fs),
                                  y=random.randint(0, fs),
                                  rotation=random.choice((3, 6, 9, 12))
                                  ) for _ in range(10)]

            for s in p2_ships:
                s.save()

            ships = [p1_ships, p2_ships]

            print(ships)

            game = Logs.Game(player1_id=player1.id,
                             player2_id=player2.id,
                             environment=env.id,
                             starting_player=starting_player,
                             start_time=start_time,
                             player1_field=p1_ships,
                             player2_field=p2_ships)

            print(game)

            p = starting_player
            time = start_time

            for _ in range(random.randint(30, 50)):
                player = players[p]
                ship = ships[p]

                att = random.choice(ship)
                ax = att.x
                ay = att.y

                tx = random.randint(0, fs)
                ty = random.randint(0, fs)

                time = random_turn(time)

                t = Logs.Turn(player_id=player.id,
                              attacker_x=ax,
                              attacker_y=ay,
                              target_x=tx,
                              target_y=ty,
                              timestamp=time)

                t.save()

                game.turns.append(t)

                p = 1 - p

            game.save()

            print(game.id)

            ws.add(ws.Game(user_id=game.player1_id, game_id=str(game.id)))
            ws.add(ws.Game(user_id=game.player2_id, game_id=str(game.id)))

        ws.commit()
