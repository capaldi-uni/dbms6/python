from flask import Flask
from flask_restful import Api

app = Flask('rest_api')
api = Api(app)

from . import resources as _r


# Main part
api.add_resource(_r.IndexRes, '/api')

api.add_resource(_r.LoginRes, '/api/login')
api.add_resource(_r.SignupRes, '/api/signup')


# Database part
api.add_resource(_r.DatabaseListRes, '/api/db')

api.add_resource(_r.DatabaseRes, '/api/db/<string:database>')

api.add_resource(_r.TableRes, '/api/db/<string:database>/'
                              '<string:table_name>')

api.add_resource(_r.QueryRes, '/api/db/<string:database>/'
                              'query/<string:query_name>')
