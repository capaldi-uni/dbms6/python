from flask import request
from flask_restful import Resource

from sqlalchemy.orm.exc import FlushError, NoResultFound
from mongoengine import ValidationError, OperationError

from api import WarshipsFactory, ContentFactory, LogRepository

Warships = WarshipsFactory()
Content = ContentFactory()
Log = LogRepository()


class IndexRes(Resource):
    def get(self):
        return {
            'login': '/api/login',
            'signup': '/api/signup',
            'database': '/api/db'
        }


class LoginRes(Resource):
    def post(self):
        try:
            nickname = request.json['nickname']
            password = request.json['password']
        except KeyError:
            return {'status': 1, 'message': 'Bad request'}, 400

        with Warships() as ws:
            try:
                user = ws.users.where(ws.User.nickname == nickname).one()
            except NoResultFound:
                return {
                    'status': 2,
                    'message': 'Incorrect nickname or password'
                }

            if user.check_password(password):
                return {'status': 0, 'token': 'SuperSecretToken'}
            else:
                return {
                    'status': 2,
                    'message': 'Incorrect nickname or password'
                }


class SignupRes(Resource):
    def post(self):
        try:
            nickname = request.json['nickname']
            password = request.json['password']
        except KeyError:
            return {'status': 1, 'message': 'Bad request'}, 400

        with Warships() as ws:
            if ws.user_exists(nickname):
                return {'status': 2, 'message': 'User already exists'}

            user = ws.User.new(nickname, password)

            ws.add(user)
            ws.commit()

            return {'status': 0, 'token': 'NewSuperSecretToken'}


class DatabaseListRes(Resource):
    def get(self):
        return {'databases': [
            {'name': 'warships', 'url': '/api/db/Warships'},
            {'name': 'content', 'url': '/api/db/Content'},
            {'name': 'log', 'url': '/api/db/Log'}
        ]}


class DatabaseRes(Resource):
    def get(self, database):
        if database in {'Warships', 'Content'}:
            if database == 'Warships':
                conn = Warships()

                queries = [
                    {
                        'name': 'feedback',
                        'description': 'Get relevant feedback',
                        'url': '/api/db/Warships/query/feedback'
                    },
                    {
                        'name': 'game_count',
                        'description': 'Get game counts for users',
                        'url': '/api/db/Warships/query/game_count'
                    },
                    {
                        'name': 'blacklist',
                        'description': 'Get ban reasons for blacklisted users',
                        'url': '/api/db/Warships/query/blacklist'
                    },
                    {
                        'name': 'user_exists',
                        'description': 'Check if user with nickname exists',
                        'url': '/api/db/Warships/query/user_exists'
                    }
                ]

            else:
                conn = Content()

                queries = [
                    {
                        'name': 'ammunition',
                        'description': 'View ammunition of ship',
                        'url': '/api/db/Content/query/ammunition'
                    },
                    {
                        'name': 'colored_ships',
                        'description': 'View ships of given color',
                        'url': '/api/db/Content/query/colored_ships'
                    },
                    {
                        'name': 'environment_ships',
                        'description': 'View ships of given environment',
                        'url': '/api/db/Content/query/environment_ships'
                    }
                ]

            tables = [{'name': name, 'url': f'/api/db/{database}/{name}'}
                      for name in conn.tables]

            return {'name': database, 'tables': tables, 'queries': queries}

        elif database == 'Log':
            return {
                'name': database,
                'tables': [
                    {
                        'name': name,
                        'url': f'/api/db/{database}/{name}'
                    }
                    for name in Log.tables
                ],
                'queries': [
                    {
                        'name': 'longest_game',
                        'description': 'Find longest game',
                        'url': '/api/db/Log/query/longest_game'
                    },
                    {
                        'name': 'shortest_game',
                        'description': 'Find game with least turns',
                        'url': '/api/db/Log/query/shortest_game'
                    }
                ]
            }

        else:
            return {'message': 'Database not found'}, 404


class TableRes(Resource):
    def get(self, database, table_name):
        if database in {'Warships', 'Content'}:
            if database == 'Warships':
                factory = Warships
            else:
                factory = Content

            with factory() as conn:

                if table_name not in conn.tables:
                    return {'message': 'Table not found'}, 404

                table = conn[table_name]
                content = [r.dict for r in conn.query(table).all()]

                return {'database': database, 'name': table_name,
                        'content': content}

        elif database == 'Log':
            with Warships() as ws, Content() as ct, Log(ws, ct):

                if table_name not in Log.tables:
                    return {'message': 'Table not found'}, 404

                table = Log[table_name]

                content = [r.dict for r in table.objects]

                return {'database': database, 'name': table_name,
                        'content': content}

        else:
            return {'message': 'Database not found'}, 404

    def post(self, database, table_name):
        data = request.json
        keys = set(data.keys())

        if database in {'Warships', 'Content'}:
            if database == 'Warships':
                factory = Warships
            else:
                factory = Content

            with factory() as conn:
                if table_name not in conn.tables:
                    return {'message': 'Table not found'}, 404

                table = conn[table_name]

                col_names = table.field_names()

                if col_names != keys:
                    resp = {'message': f'Wrong set of keys'}

                    if extra := keys - col_names:
                        resp['extra'] = list(extra)

                    if missing := col_names - keys:
                        resp['missing'] = list(missing)

                    return resp, 400

                try:
                    conn.add(table(**data))
                    conn.commit()

                    return {'message': 'Record was created'}, 201

                except FlushError as e:
                    return {'message': 'Unable to create a record',
                            'exception': str(e)}, 409

        elif database == 'Log':
            with Warships() as ws, Content() as ct, Log(ws, ct):

                if table_name not in Log.tables:
                    return {'message': 'Table not found'}, 404

                table = Log[table_name]

                col_names = table.field_names()

                if col_names != keys:
                    resp = {'message': f'Wrong set of keys'}

                    if extra := keys - col_names:
                        resp['extra'] = list(extra)

                    if missing := col_names - keys:
                        resp['missing'] = list(missing)

                    return resp, 400

                try:
                    record = table(**data)
                    record.save()

                    return {'message': 'Record was created',
                            'id': str(record.id)}, 201

                except ValidationError as e:
                    return {'message': 'Unable to create a record',
                            'exception': str(e)}, 409
        else:
            return {'message': 'Database not found'}, 404

    def put(self, database, table_name):
        data = request.json
        if database in {'Warships', 'Content'}:

            try:
                key = data['key']
                key_name = key['name']
                key_value = key['value']

                fields = data['fields']
            except KeyError:
                return {'message': 'Wrong body format'}, 400

            if database == 'Warships':
                factory = Warships
            else:
                factory = Content

            with factory() as conn:

                if table_name not in conn.tables:
                    return {'message': 'Table not found'}, 404

                table = conn[table_name]

                records = [rec for rec in conn.query(table).all()
                           if rec[key_name] == key_value]

                if len(records) > 1:
                    return {'message': 'Key matches multiple records'}, 400

                if len(records) == 0:
                    return {'message': 'Specified record not found'}, 404

                record = records[0]

                col_names = table.field_names()

                if extra := set(fields.keys()) - col_names:
                    return {'message': 'Unknown keys',
                            'keys': list(extra)}, 400

                try:
                    for key in fields:
                        record[key] = fields[key]
                    conn.commit()

                    return {'message': 'Record was updated'}, 200

                except FlushError as e:
                    return {'message': 'Unable to update record',
                            'exception': str(e)}, 409

        elif database == 'Log':
            try:
                rec_id = data['id']

                fields = data['fields']
            except KeyError:
                return {'message': 'Wrong body format'}, 400

            with Warships() as ws, Content() as ct, Log(ws, ct):

                if table_name not in Log.tables:
                    return {'message': 'Table not found'}, 404

                table = Log[table_name]

                try:
                    record = table.objects(id=rec_id).get()

                except table.DoesNotExist:
                    return {'message': 'Specified record not found'}, 404

                col_names = table.field_names()

                if extra := set(fields.keys()) - col_names:
                    return {'message': 'Unknown keys',
                            'keys': list(extra)}, 400

                try:
                    for key in fields:
                        record[key] = fields[key]

                    record.save()

                    return {'message': 'Record was updated'}, 200

                except ValidationError as e:
                    return {'message': 'Unable to update record',
                            'exception': str(e)}, 409

        else:
            return {'message': 'Database not found'}, 404

    def delete(self, database, table_name):
        data = request.json

        if database in {'Warships', 'Content'}:
            try:
                key = data['key']
                key_name = key['name']
                key_value = key['value']
            except KeyError:
                return {'message': 'Wrong body format'}, 400

            if database == 'Warships':
                factory = Warships
            else:
                factory = Content

            with factory() as conn:

                if table_name not in conn.tables:
                    return {'message': 'Table not found'}, 404

                table = conn[table_name]

                records = [rec for rec in conn.query(table).all()
                           if rec[key_name] == key_value]

                if len(records) > 1:
                    return {'message': 'Key matches multiple records'}, 400

                if len(records) == 0:
                    return {'message': 'Specified record not found'}, 404

                record = records[0]

                try:
                    if record.can_delete:
                        conn.delete(record)
                        conn.commit()
                    else:
                        raise RuntimeError('Cannot delete record')

                    return {'message': 'Record was deleted'}, 200

                except (FlushError, RuntimeError) as e:
                    return {'message': 'Unable delete record',
                            'exception': str(e)}, 409

        elif database == 'Log':
            try:
                rec_id = data['id']
            except KeyError:
                return {'message': 'Wrong body format'}, 400

            with Warships() as ws, Content() as ct, Log(ws, ct):

                if table_name not in Log.tables:
                    return {'message': 'Table not found'}, 404

                table = Log[table_name]

                try:
                    record = table.objects(id=rec_id).get()

                except table.DoesNotExist:
                    return {'message': 'Specified record not found'}, 404

                try:
                    if isinstance(record, Log.Game):
                        Log.delete_game(record)
                    else:
                        record.delete()

                    return {'message': 'Record was deleted'}, 200

                except OperationError as e:
                    return {'message': 'Unable delete record',
                            'exception': str(e)}, 409

        else:
            return {'message': 'Database not found'}, 404


class QueryRes(Resource):
    def get(self, database, query_name):
        if database == 'Warships':
            with Warships() as conn:
                if query_name in {'feedback', 'game_count', 'blacklist'}:
                    if query_name == 'feedback':
                        cursor = conn.get_relevant_feedback()

                    elif query_name == 'game_count':
                        cursor = conn.get_game_count()

                    else:
                        cursor = conn.get_ban_reasons()

                    return {
                        'query': query_name,
                        'data': [
                            {key: row[key] for key in row.keys()}
                            for row in cursor
                        ]
                    }

                elif query_name == 'user_exists':
                    nickname = request.args.get('nickname', None)

                    if nickname is None:
                        return {'message': 'nickname is a required param'}, 400

                    exists = conn.user_exists(nickname)

                    return {
                        'query': query_name,
                        'nickname': nickname,
                        'exists': exists
                    }

                else:
                    return {'message': 'No such request'}, 404

        elif database == 'Content':
            with Content() as conn:
                if query_name in {'ammunition', 'colored_ships',
                                  'environment_ships'}:

                    name = request.args.get('name', None)

                    if name is None:
                        return {'message': 'name is a required param'}, 400

                    if query_name == 'ammunition':
                        cursor = conn.ammunition_of_ship(name)

                    elif query_name == 'colored_ships':
                        cursor = conn.get_ships_of_color(name)

                    else:
                        cursor = conn.ships_of_environment(name)

                    return {
                        'query': query_name,
                        'data': [
                            {key: row[key] for key in row.keys()}
                            for row in cursor
                        ]
                    }

                else:
                    return {'message': 'No such request'}, 404

        elif database == 'Log':
            with Warships() as ws, Content() as ct, Log(ws, ct):
                if query_name == 'longest_game':
                    game = Log.find_longest_game()

                    return {'query': query_name, 'game': game.dict}

                elif query_name == 'shortest_game':
                    game = Log.find_least_turns()

                    return {'query': query_name, 'game': game.dict}

                else:
                    return {'message': 'No such request'}, 404

        else:
            return {'message': 'Database not found'}, 404
